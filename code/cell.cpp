#include "cell.hpp"

cell::cell (int w, bool t, int ta, bool p)// constructor cell
{
weather = w;//posos kairos
treasure = t;//an yparxei thhsauros
treasureAmount = ta;//posos thhsauros
port = p;//an yparxei limani
}

//methodoi get gia tis tessesris metavlhtes
int 	cell::getWeather() const 
			{ return weather; } 
bool 	cell::getTreasure() const 
			{ return treasure; }
int 	cell::getTreasureAmount() const 
			{ return treasureAmount; }
bool 	cell::getPort() const 
			{ return port; }   
			
//methodoi set gia tis tessesris metavlhtes
void 	cell::setWeather( int w) 	
			{ weather=w;}
void 	cell::setTreasure( bool t) 	
			{ treasure=t;}  
void 	cell::setTreasureAmount( int ta) 	
			{ treasureAmount=ta;}
void 	cell::setPort( bool p) 	
			{ port=p;}  