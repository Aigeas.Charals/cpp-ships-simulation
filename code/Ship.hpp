#ifndef Ship_hpp 
#define Ship_hpp 

#include "header.hpp"
#include "cell.hpp"

int x, y, i;

class Ship{ 
  public:  
    Ship ( );
	~Ship();
	char getCategory() const;
	int getId() const;
	int getCurrentLife() const; 
	int getMaxLife() const; 
	int getTreasure() const;  
	int getSpeed() const; 
	int getPositionx() const; 
	int getPositiony() const; 
	int getMaxTreasure() const; 
	bool getMoving() const;
	void print() ; 
	void setCategory (char) ;
	void setId(int);
	void setCurrentLife ( int ); 
	void setTreasure ( int );  
	void setMaxLife( int ) ; 
	void setSpeed( int ) ; 
	void setMaxTreasure( int ) ; 
	virtual void operation(int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2])  = 0;
	void setX(int);
	void setY(int);
	void setTotalMoves(int);
	void setMoving(bool);
	int getX() const;
	int getY() const;
	int getTotalMoves() const;
	void movement( vector<Ship*> &allShips, int i,  bool reserved[DIMENSION1][DIMENSION2] );
	int getDamage() const;
    void setDamage( int );
   
  protected: 
	   char Category;
	   int Id;
       int CurrentLife; 
       int Treasure; 
	   int MaxLife;
	   int Speed;
	   int MaxTreasure;
       int positionx; 
	   int positiony;
	   int totalMoves;
	   bool moving;
	   int move;
	   int moveY;
	   int moveX;
	   int temp;
	   int Damage; 
	   char shipCategoryIn;
	   int shipIdIn;
	   int j;
	   int new_life;
	   int temp_treasure;
	   int new_treasure;

}; 


class Explore : public Ship { 
  public: 
    Explore(int ID,  bool reserved[DIMENSION1][DIMENSION2]);
	void operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]); 
};


class Merchant : public Ship { 
  public: 
    Merchant (int ID,  bool reserved[DIMENSION1][DIMENSION2]);    
    void operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]); 
    
};

class Pirate : public Ship { 
  public: 
   Pirate(int ID,  bool reserved[DIMENSION1][DIMENSION2]);
   void operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]); 
   
   int getPower() const ; 
   void setPower ( int ); 

 private: 
    int Power;
}; 
	 
	 
class Repair : public Ship { 
  public: 
    Repair (int ID,  bool reserved[DIMENSION1][DIMENSION2]);
    void operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]); 
    
};
#endif
