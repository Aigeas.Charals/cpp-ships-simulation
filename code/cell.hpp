#ifndef cell_hpp 
#define cell_hpp 

class cell{ 
 public: 
    cell ( int w, bool t, int ta, bool p);
    
    int getWeather() const; 
	bool getTreasure() const; 
	int getTreasureAmount() const;  
	bool getPort() const; 
	
	void setWeather( int ) ;
	void setTreasure ( bool ); 
	void setTreasureAmount ( int );  
	void setPort( bool ) ;

protected: 
	int weather;
	bool treasure;
	int treasureAmount;
	bool port;

};
#endif
