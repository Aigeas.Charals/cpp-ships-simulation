#include "Ship.hpp"
#include "Ship.hpp"


Ship::Ship () {}//constructor Ship
			
         
Ship::~Ship() //distructor Ship
			{ cout <<" DESTRUCTION OF A SHIP "; };

//methodoi get gia tis metavlhtes
char 	Ship::getCategory()  const 
			{ return Category; } 
int 	Ship::getId() const 
			{ return Id; } 
int 	Ship::getMaxLife() const 
			{ return MaxLife; } 
int 	Ship::getCurrentLife() const 
			{ return CurrentLife; } 
int 	Ship::getSpeed() const 
			{ return Speed; } 
int 	Ship::getMaxTreasure() const 
			{ return MaxTreasure; } 			
int 	Ship::getTreasure()  const 
			{ return Treasure; }
int 	Ship::getX() const
			{ return positionx; }
int 	Ship::getY( ) const
			{ return positiony; }
int 	Ship::getTotalMoves( ) const
			{ return totalMoves; }
bool 	Ship::getMoving( ) const
			{ return moving; }
			
//methodoi set gia tis metavlhtes
void 	Ship::setX(int Xin)
			{ positionx=Xin;}		
void 	Ship::setY(int Yin)
			{ positiony=Yin;}
void 	Ship::setCategory( char C) 	
			{ Category=C; } 
void 	Ship::setId( int ID) 	
			{ Id=ID; } 		
void 	Ship::setCurrentLife( int LF) 	
			{ CurrentLife=LF;} 
void 	Ship::setSpeed( int S) 
			{ Speed=S; } 
void 	Ship::setMaxTreasure( int MT) 
			{ MaxTreasure=MT; } 			
void 	Ship::setTreasure(  int T) 	
			{ Treasure=T; }
void 	Ship::setMaxLife ( int MF)
			{ MaxLife=MF; } 
void 	Ship::setTotalMoves ( int TM)
			{ totalMoves=TM; }
void 	Ship::setMoving ( bool M)
			{ moving=M; }	

//methodos emfanishs plhroforiwn			
void 	Ship::print() { 
    cout <<setw(2)<<Category<<setw(3)<<Id<<setw(4)<<MaxLife<<setw(4)
         <<CurrentLife<<setw(2)<<Speed<<setw(3)<<MaxTreasure<<setw(3)<<Treasure<<endl; 
}
//kinhsh ploiwn
void Ship::movement( vector<Ship*> &allShips, int i, bool reserved[DIMENSION1][DIMENSION2] )
	{
		move = rand() % 4;
		allShips[i]->setMoving(false);
		if (move == 0) //panw
			{
				if  (allShips[i]->getY() - allShips[i]->getSpeed()  < 0) //elegxos gia panw orio xarth
				{
					moveY = allShips[i]->getY() + allShips[i]->getSpeed(); //an den mporei na paei panw paei katw
					if ( reserved[allShips[i]->getX()][moveY] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setY(moveY);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);
					}
				}			
				else
				{					
					moveY = allShips[i]->getY() - allShips[i]->getSpeed(); //mporei na paei panw
					if ( reserved[allShips[i]->getX()][moveY] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setY(moveY);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);
					}
			

				}
			}
			if (move == 1) //dexia
			{
				if  (allShips[i]->getX() + allShips[i]->getSpeed()  >= DIMENSION1) //elegxos gia dexia orio xarth
				{
					moveX = allShips[i]->getX() - allShips[i]->getSpeed(); //an den paei dexia paei aristera
					if ( reserved[moveX][allShips[i]->getX()] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setY(moveX);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);
					}
				}			
				else
				{					
					moveX = allShips[i]->getX() + allShips[i]->getSpeed(); //mporei na paei dexia
					if ( reserved[moveX][allShips[i]->getY()] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setX(moveX);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);
					}
			

				}
			}
			if (move == 2) //katw
			{
				if  (allShips[i]->getY() + allShips[i]->getSpeed()  >= DIMENSION2) //elegxos gia katw orio xarth
				{
					moveY = allShips[i]->getY() - allShips[i]->getSpeed(); //an den mporei na paei katw paei panw
					if ( reserved[allShips[i]->getX()][moveY] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setY(moveY);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					}
				}			
				else
				{					
					moveY = allShips[i]->getY() + allShips[i]->getSpeed(); //mporei na paei katw
					if ( reserved[allShips[i]->getX()][moveY] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setY(moveY);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					}
			

				}
			}
			if (move == 3) //aristera
			{
				if  (allShips[i]->getX() - allShips[i]->getSpeed()  < 0) //elegxos gia aristera orio xarth
				{
					moveX = allShips[i]->getX() + allShips[i]->getSpeed(); //an den mporei na paei aristera paei dexia
					if ( reserved[moveX][allShips[i]->getX()] == false )//an einai eleutherh h thesh
					{	
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;//eleutherwse prohgoumenh thesh
						allShips[i]->setY(moveX);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					}
				}			
				else
				{					
					moveX = allShips[i]->getX() - allShips[i]->getSpeed(); //mporei na paei aristera
					if ( reserved[moveX][allShips[i]->getY()] == false )//an einai eleutherh h thesh
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh					
						allShips[i]->setX(moveX);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);//energopoihse flag oti exei kinhthei
					}
			

				}
			}
			reserved[allShips[i]->getX()][allShips[i]->getY()] = true;//kane reserved th nea thesh

	
	}

//methodoi get kai set
int  Ship::getDamage() const 
 		{ return Damage; } 
void Ship::setDamage ( int D )
 		{ Damage=D; }

Pirate::Pirate(int ID,  bool reserved[DIMENSION1][DIMENSION2])//constructor pirate
			  {
			  	Category = 'P';
				Id = ID;
				MaxLife =  12;
				Speed = 2;
				CurrentLife = 12;
				MaxTreasure = 10;
				Treasure = 1;
				Power = 2;
				srand (time(NULL));//topothethsh se thesh tou xarth
 				positionx= ( rand()% DIMENSION1);
 				positiony= ( rand()% DIMENSION2);
 				while (reserved[positionx][positiony] == true) 
    				{
       					 positionx = rand() % DIMENSION1; 
       					 positiony = rand() % DIMENSION2; 
   					 }
				reserved[positionx][positiony] = true;//kane reserved th thesh
			    } 
			  
Explore::Explore(int ID,  bool reserved[DIMENSION1][DIMENSION2])//constructor explore
			  {
			  	Category = 'E';
				Id = ID;
				MaxLife =  12;
				Speed = 2;
				CurrentLife = 12;
				MaxTreasure = 10;
				Treasure = 1;
				srand (time(NULL));//topothethsh se thesh tou xarth
  				positionx= ( rand()% DIMENSION1);
 				positiony= ( rand()% DIMENSION2);
 				while (reserved[positionx][positiony] == true) 
    				{
       					 positionx = rand() % DIMENSION1; 
       					 positiony = rand() % DIMENSION2; 
   					 }
				reserved[positionx][positiony] = true;//kane reserved th thesh
				} 
			
			
Merchant::Merchant (int ID,  bool reserved[DIMENSION1][DIMENSION2])//constructor merchant
			  {
			  	Category = 'M';
				Id = ID;
				MaxLife =  12;
				Speed = 2;
				CurrentLife = 12;
				MaxTreasure = 10;
				Treasure = 1;
				srand (time(NULL));//topothethsh se thesh tou xarth
 				positionx= ( rand()% DIMENSION1);
 				positiony= ( rand()% DIMENSION2);
 				while (reserved[positionx][positiony] == true) 
    				{
       					 positionx = rand() % DIMENSION1; 
       					 positiony = rand() % DIMENSION2; 
   					 }
				reserved[positionx][positiony] = true;//kane reserved th thesh
				
				} 
			
Repair::Repair (int ID,  bool reserved[DIMENSION1][DIMENSION2])//constructor repair
			  {
			  	Category = 'R';
				Id = ID;
				MaxLife =  12;
				Speed = 2;
				CurrentLife = 12;
				MaxTreasure = 10;
				Treasure = 1;
				srand (time(NULL));//topothethsh se thesh tou xarth
 				positionx= ( rand()% DIMENSION1);
 				positiony= ( rand()% DIMENSION2);
 				while (reserved[positionx][positiony] == true) 
    				{
       					 positionx = rand() % DIMENSION1; 
       					 positiony = rand() % DIMENSION2; 
   					 }
				reserved[positionx][positiony] = true;//kane reserved th thesh
				} 

//operation tou peiratikou ploiou				
void Pirate::operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]){ 
    if (allShips[i]->getCategory() == 'P' && allShips[i]->getMoving()  == true)
	{
		//elegxos gia yparxh ploiou j se geitonikh thesh
		for (j = 0; j < allShips.size(); j++)
		{	
			//to ploio j einai aristera
			if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 0) && (allShips[j]->getCategory() != 'P') ) 
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3; //klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; 
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
				
			// to ploio j einai panw aristera
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 1) && (allShips[j]->getCategory() != 'P') ) 
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; 
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
			
			// to ploio j einai panw
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 0) && ( (allShips[i]->getY() - allShips[j]->getY()) == 1) && (allShips[j]->getCategory() != 'P') )
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; //meiwsh thhsaurou allou ploiou
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
			
			//to ploio j einai panw dexia
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == -1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 1) && (allShips[j]->getCategory() != 'P') )
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; //meiwsh thhsaurou allou ploiou
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
			
			//to ploio j einai dexia
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == -1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 0) && (allShips[j]->getCategory() != 'P') )
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; //meiwsh thhsaurou allou ploiou
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
			
			//to ploio j einai katw dexia
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == -1) && ( (allShips[i]->getY() - allShips[j]->getY()) == -1) && (allShips[j]->getCategory() != 'P') )
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; //meiwsh thhsaurou allou ploiou
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
			
			//to ploio j einai katw
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 0) && ( (allShips[i]->getY() - allShips[j]->getY()) == -1) && (allShips[j]->getCategory() != 'P') )
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; //meiwsh thhsaurou allou ploiou
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}
			
			//to ploio j einai katw aristera
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1) && ( (allShips[i]->getY()  - allShips[j]->getY()) == -1) && (allShips[j]->getCategory() != 'P') )			
			{
				new_life= allShips[j]->getCurrentLife() - 2; //meiwsh antoxhs allou ploiou
				allShips[j]->setCurrentLife(new_life); 
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId()<<" CAUSES 2 DAMAGE TO SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;
				Sleep(500);
				temp_treasure = allShips[j]->getTreasure()/3;//klepsimo thhsaurou allou ploiou
				new_treasure = allShips[j]->getTreasure() - temp_treasure; //meiwsh thhsaurou allou ploiou
				allShips[j]->setTreasure(new_treasure); 
				new_treasure = allShips[i]->getTreasure() + temp_treasure; //aukshsh thhsaurou peiratikou ploiou
				allShips[i]->setTreasure(new_treasure);
				cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" TAKES 30% TREASURE FROM THE SHIP "<< allShips[j]->getCategory() << allShips[j]->getId() <<endl;	
				Sleep(500);	
			}	
		}	
	}
}
	
//methodoi get kai set
 int  Pirate::getPower() const 
 		{ return Power; } 
 void Pirate::setPower ( int P )
 		{ Power=P; }

			

			
//operation tou explore ploiou       
void Explore::operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]){ 
	if (allShips[i]->getCategory() == 'E' && allShips[i]->getMoving()  == true)
	{
		//deksia kakos kairos
		if  (allShips[i]->getX() + 1 < DIMENSION1){
			if (position[getX() + 1][getY()]->getWeather() >6){
				if  (allShips[i]->getX() - 1 >= 0){
					if ( reserved[getX() - 1][getY()] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setX(getX()-1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
						Sleep(500);
					}
				}	
			}
		}
		
		//aristera kakos kairos
		else if  (allShips[i]->getX() - 1 >= 0){
			if (position[getX() - 1][getY()]->getWeather() >6){
				if  (allShips[i]->getX() + 1 < DIMENSION1){
					if ( reserved[getX() + 1][getY()] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setX(getX()+1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
						Sleep(500);
					}
				}				
			}	
		}	

		//panw kakos kairos
		else if  (allShips[i]->getY() - 1 >= 0){
			if (position[getX()][getY() - 1]->getWeather() >6){
				if  (allShips[i]->getY() + 1 < DIMENSION2){
					if ( reserved[getX()][getY() + 1] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setY(getY()+1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
						Sleep(500);
					}
				}
			}	
		}

		//katw kakos kairos
		else if  (allShips[i]->getY() + 1 < DIMENSION2){
			if (position[getX()][getY() + 1]->getWeather() >6){
				if  (allShips[i]->getY() - 1 >=0){
					if ( reserved[getX()][getY() - 1] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setY(getY()-1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
						Sleep(500);
					}
				}
			}	
		}
		
		//deksia panw kakos kairos
		else if  ((allShips[i]->getX() + 1 < DIMENSION1)&&(allShips[i]->getY() - 1 >= 0)){
			if (position[getX() + 1][getY()-1]->getWeather() >6){
				if (( reserved[getX()][getY() + 1] == false )&&(allShips[i]->getY() + 1 < DIMENSION2)) //elegxos gia katw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(getY()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
				else if (( reserved[getX() - 1][getY()] == false )&&(allShips[i]->getX() - 1 >= 0)) //elegxos gia aristera orio xarth 
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(getX()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
			}	
		}
		
		//deksia katw kakos kairos
		else if  ((allShips[i]->getX() + 1 < DIMENSION1)&&(allShips[i]->getY() + 1 < DIMENSION2)){
			if (position[getX() + 1][getY()+1]->getWeather() >6){
				if (( reserved[getX()][getY() - 1] == false )&&(allShips[i]->getY() - 1 >=0)) //elegxos gia panw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(getY()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
				else if (( reserved[getX() - 1][getY()] == false )&&(allShips[i]->getX() - 1 >= 0)) //elegxos gia aristera orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(getX()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}	 
			}	
		}
		
		//aristera panw kakos kairos
		else if  ((allShips[i]->getX() - 1 >= 0)&&(allShips[i]->getY() - 1 >= 0)){
			if (position[getX() - 1][getY()- 1]->getWeather() >6){
				if (( reserved[getX()][getY() + 1] == false )&&(allShips[i]->getY() + 1 < DIMENSION2)) //elegxos gia katw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(getY()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
				else if (( reserved[getX() + 1][getY()] == false )&&(allShips[i]->getX() + 1 <DIMENSION1)) //elegxos gia dexia orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(getX()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}	
			}	
		}	
		
		//aristera katw kakos kairos
		else if  ((allShips[i]->getX() - 1 >= 0)&&(allShips[i]->getY() + 1 < DIMENSION2)){
			if (position[getX() - 1][getY()+ 1]->getWeather() >6){
				if (( reserved[getX()][getY() - 1] == false )&&(allShips[i]->getY() - 1 >= 0)) //elegxos gia panw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(getY()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
				else if (( reserved[getX() + 1][getY()] == false )&&(allShips[i]->getX() + 1 <DIMENSION1)) //elegxos gia dexia orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(getX()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}	
			}	
		}	
	}	 
	reserved[allShips[i]->getX()][allShips[i]->getY()] = true;//kane reserved th nea thesh
	
	//elegxos gia yparxh piratikou ploiou se geitonikh thesh
	for (j = 0; j < allShips.size(); j++)
		{	
			//to peiratiko ploio j einai aristera
			if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 0) && (allShips[j]->getCategory() == 'P') ) 
			{
				if  (allShips[i]->getX() + 1 < DIMENSION1){ //elegxos gia dexia orio xarth
					if ( reserved[allShips[i]->getX()+1][allShips[i]->getY()] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setX(allShips[i]->getX()+1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
						Sleep(500);
					}
				}
			}
				
			// to peiratiko ploio j einai panw aristera
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 1) && (allShips[j]->getCategory() == 'P') ) 
			{
				if (( reserved[allShips[i]->getX()][allShips[i]->getY() + 1] == false )&&(allShips[i]->getY() + 1 < DIMENSION2)) //elegxos gia katw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(allShips[i]->getY()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
					Sleep(500);
				}
				else if (( reserved[allShips[i]->getX() + 1][allShips[i]->getY()] == false )&&(allShips[i]->getX() + 1 <DIMENSION1)) //elegxos gia dexia orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(allShips[i]->getX()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
					Sleep(500);
				}
			}
			
			// to peiratiko ploio j einai panw
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 0) && ( (allShips[i]->getY() - allShips[j]->getY()) == 1) && (allShips[j]->getCategory() == 'P') )
			{
				if  (allShips[i]->getY() + 1 < DIMENSION2){ //elegxos gia katw orio xarth
					if ( reserved[allShips[i]->getX()][allShips[i]->getY() + 1] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setY(allShips[i]->getY()+1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
						Sleep(500);
					}
				}
			}
			
			//to ploio j einai panw dexia
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == -1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 1) && (allShips[j]->getCategory() == 'P') )
			{
				if (( reserved[allShips[i]->getX()][allShips[i]->getY() + 1] == false )&&(allShips[i]->getY() + 1 < DIMENSION2)) //elegxos gia katw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(allShips[i]->getY()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
					Sleep(500);
				}
				else if (( reserved[allShips[i]->getX() - 1][allShips[i]->getY()] == false )&&(allShips[i]->getX() - 1 >= 0)) //elegxos gia aristera orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(allShips[i]->getX()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
					Sleep(500);
				}
			}
			//to ploio j einai dexia
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == -1) && ( (allShips[i]->getY() - allShips[j]->getY()) == 0) && (allShips[j]->getCategory() == 'P') )
			{
				if  (allShips[i]->getX() - 1 >= 0){ //elegxos gia aristera orio xarth
					if ( reserved[allShips[i]->getX() - 1][allShips[i]->getY()] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setX(allShips[i]->getX()-1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
						Sleep(500);
					}
				}
			}
			//to ploio j einai katw dexia
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == -1) && ( (allShips[i]->getY() - allShips[j]->getY()) == -1) && (allShips[j]->getCategory() == 'P') )
			{
				if (( reserved[allShips[i]->getX()][allShips[i]->getY() - 1] == false )&&(allShips[i]->getY() - 1 >=0)) //elegxos gia panw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(allShips[i]->getY()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
					Sleep(500);
				}
				else if (( reserved[allShips[i]->getX() - 1][allShips[i]->getY()] == false )&&(allShips[i]->getX() - 1 >= 0)) //elegxos gia aristera orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(allShips[i]->getX()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
					Sleep(500);
				}
			}
			//to ploio j einai katw
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 0) && ( (allShips[i]->getY() - allShips[j]->getY()) == -1) && (allShips[j]->getCategory() == 'P') )
			{
				if  (allShips[i]->getY() - 1 >=0){ //elegxos gia panw orio xarth
					if ( reserved[allShips[i]->getX()][allShips[i]->getY() - 1] == false )
					{
						reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
						allShips[i]->setY(allShips[i]->getY()-1);//allakse syntetagmenes
						temp = allShips[i]->getTotalMoves();
						temp++;
						allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
						allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
						cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD PIRATES " << endl;
						Sleep(500);
					}
				}
			}
			//to ploio j einai katw aristera
			else if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1) && ( (allShips[i]->getY()  - allShips[j]->getY()) == -1) && (allShips[j]->getCategory() == 'P') )			
			{
				if (( reserved[allShips[i]->getX()][allShips[i]->getY() - 1] == false )&&(allShips[i]->getY() - 1 >= 0)) //elegxos gia panw orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setY(allShips[i]->getY()-1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
				else if (( reserved[allShips[i]->getX() + 1][allShips[i]->getY()] == false )&&(allShips[i]->getX() + 1 <DIMENSION1)) //elegxos gia dexia orio xarth
				{
					reserved[allShips[i]->getX()][allShips[i]->getY()]== false;	//eleutherwse prohgoumenh thesh	
					allShips[i]->setX(allShips[i]->getX()+1);//allakse syntetagmenes
					temp = allShips[i]->getTotalMoves();
					temp++;
					allShips[i]->setTotalMoves(temp);//metrhsh kinhsewn
					allShips[i]->setMoving(true);// energopoihse flag oti exei kinhthei
					cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS CHANGED POSITION BECAUSE OF BAD WEATHER " << endl;
					Sleep(500);
				}
			}	
		}
	reserved[allShips[i]->getX()][allShips[i]->getY()] = true;	//kane reserved th nea thesh	
	}


//operation tou merchant ploiou	         
void Merchant::operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]){ 
 	if ((allShips[i]->getCategory() == 'M' && allShips[i]->getMoving()  == true))
	{
		
		//deksia limani
		if  (allShips[i]->getX() + 1 < DIMENSION1){
			if (position[getX() + 1][getY()]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}
		
		//aristera limani
		if  (allShips[i]->getX() - 1 >= 0){
			if (position[getX() - 1][getY()]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}	

		//panw limani
		if  (allShips[i]->getY() - 1 >= 0){
			if (position[getX()][getY() - 1]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}

		//katw limani
		if  (allShips[i]->getY() + 1 < DIMENSION2){
			if (position[getX()][getY() + 1]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}
		
		//deksia panw limani
		if  ((allShips[i]->getX() + 1 < DIMENSION1)&&(allShips[i]->getY() - 1 >= 0)){
			if (position[getX() + 1][getY()-1]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}
		
		//deksia katw limani
		if  ((allShips[i]->getX() + 1 < DIMENSION1)&&(allShips[i]->getY() + 1 < DIMENSION2)){
			if (position[getX() + 1][getY()+1]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}
		
		
		//aristera panw limani
		if  ((allShips[i]->getX() - 1 >= 0)&&(allShips[i]->getY() - 1 >= 0)){
			if (position[getX() - 1][getY()- 1]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}	
		
		//aristera katw limani
		if  ((allShips[i]->getX() - 1 >= 0)&&(allShips[i]->getY() + 1 < DIMENSION2)){
			if (position[getX() - 1][getY()+ 1]->getPort() == true){
				temp = allShips[i]->getTreasure();
				allShips[i]->setTreasure(temp + 2);//aukshsh thhsaurou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISED ITS TREASURE BY 2 BECAUSE OF PORT " << endl;
				Sleep(500);
			}	
		}	
		
	}
		 
}


		
//operation tou repair ploiou        
void Repair::operation( int i, vector<Ship*> allShips, cell *position[DIMENSION1][DIMENSION2], bool reserved[DIMENSION1][DIMENSION2]){ 
 	if ((allShips[i]->getCategory() == 'R' && allShips[i]->getMoving()  == true)){
		for (int j = 0; j < allShips.size(); j++)
		{
			if ( ( (allShips[i]->getX() - allShips[j]->getX()) == 1 && (allShips[i]->getY() - allShips[j]->getY()) == 0 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == 1 && (allShips[i]->getY() - allShips[j]->getY()) == 1 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == 0 && (allShips[i]->getY() - allShips[j]->getY()) == 1 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == -1 && (allShips[i]->getY() - allShips[j]->getY()) == 1 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == -1 && (allShips[i]->getY() - allShips[j]->getY()) == 0 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == -1 && (allShips[i]->getY() - allShips[j]->getY()) == -1 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == 0 && (allShips[i]->getY() - allShips[j]->getY()) == -1 )
			|| ( (allShips[i]->getX() - allShips[j]->getX()) == 1 && (allShips[i]->getY() - allShips[j]->getY()) == -1 ) )
			{
				if (allShips[j]->getCurrentLife()  < allShips[j]->getMaxLife() )//an h antoxh tou allou ploiou einai mikroterh tou megistou
				{
					if (allShips[j]->getTreasure()  >= 2)//an o thhsauros tou allou ploiou einai megaluteros tou 2
					{
						int temp = allShips[j]->getTreasure();
						allShips[j]->setTreasure(temp - 2);//meiwse to thhsauro tou allou ploiou
						if (allShips[i]->getTreasure()  < 0)//aukshse to thhsauro tou repair
						{
						allShips[i]->setTreasure(0);
						}
						temp = allShips[i]->getTreasure();
						allShips[i]->setTreasure(temp + 2);
						if (allShips[i]->getTreasure()  > 20)
						{
						allShips[i]->setTreasure(20);
						}
						temp = allShips[j]->getCurrentLife();//aukshse thn antoxh tou allou ploiou
						allShips[j]->setCurrentLife(temp + 2);
						if (allShips[j]->getCurrentLife()  > 12)
						{
						allShips[j]->setCurrentLife(12);
						}
						cout <<"  SHIP " <<allShips[i]->getCategory() <<allShips[i]->getId() <<" REPAIRED " <<allShips[j]->getCategory() <<allShips[j]->getId() <<endl; 
						Sleep(500);

					}
				}
			}
		} 
	}	 
}
