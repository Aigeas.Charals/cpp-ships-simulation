#ifndef map_hpp 
#define map_hpp 

#include "header.hpp"
#include "ship.cpp"
#include "cell.cpp"
#include <iostream> 
#include <string>


class map{ 
 public: 
    map ();
	void drawMap();
	void creation();
	void application();
	void restart();
	void endApplication(int);
	string choice;
	void menu();
	void controlShips(int i);
	void shipInformation();
	void positionInformation();
	void shipCategoryInformation();
	void generalInformation();
	void createShip();
	void destroyShip();
	void editPosition();
 private:
 		bool flag;
    	bool notfound;
	   int RoundCount;
	   int numberPiratesTotal;
	   int numberMerchantsTotal;
       int numberRepairsTotal;
       int numberExplorersTotal;
	   int numberPirates;
	   int numberMerchants;
       int numberRepairs;
       int numberExplorers;
       bool tempPort;
       bool	tempTreasure;
	   int tempWeather;
	   int tempTreasureAmount;
	   int tempTotalTreasure;
	   bool reserved[DIMENSION1][DIMENSION2]; 
	   cell *position[DIMENSION1][DIMENSION2];
	   int i, j, k, x, y, l, temp, random;
	   int id, ID;
	   char C;
	   int  ML, LF, S, MT, T, P;
	   vector<Ship*> allShips;
	   int input;
	   char shipCategoryIn;
	   int shipIdIn;
	   int yIn , xIn, max;
	   int newTreasure , newWeather;
};
#endif
