#include "map.hpp"


void map::drawMap()//dhmiourgia xarth
{	
    system("cls");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_INTENSITY | 15);
    cout << endl; 
	for (j = 0; j < DIMENSION2; j++)
    {	
        for (i = 0; i < DIMENSION1; i++)
        {
			if (position[i][j]->getPort() == true) // edw uparxei ena limani
			{
			  SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BACKGROUND_INTENSITY | 15);
               cout << "--"; 
			}
			else if (reserved[i][j] == true) // ploia
			{
				notfound=true;
					for (int l = 0; l < allShips.size(); l++)
					{						
						if (allShips[l]->getX() == i && allShips[l]->getY() == j )
						{
								SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BACKGROUND_RED | 15);
								cout << allShips[l]->getCategory() << allShips[l]->getId();
								notfound=false;
						}
					}
				if (notfound==true)
					{
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_INTENSITY | BACKGROUND_GREEN | BACKGROUND_BLUE | 15);
					cout << "  ";
					}
			}			
			else if (position[i][j]->getTreasure() == true ) // edw uparxei thisauros
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_INTENSITY  | BACKGROUND_RED | BACKGROUND_GREEN| 15);
				cout << "  ";
			}
            else // edw den uparxei tipota
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_INTENSITY | BACKGROUND_GREEN | BACKGROUND_BLUE | 15);
				cout << "  "; 
			}
        }
		cout << endl;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_INTENSITY |15);
    }
	cout << endl;	
}

map::map()//constructor map
{
	RoundCount = 0;
	numberPiratesTotal= rand() % 3 + 3;// dhmiourgia arithmou ploiwn
    numberMerchantsTotal= rand() % 3 + 2;
    numberRepairsTotal= rand() % 3 + 3;
    numberExplorersTotal= rand() % 3 + 3;
	numberPirates = rand() % 3 + 3; // dhmiourgia arithmou ploiwn
	numberMerchants = rand() % 3 + 2; 
	numberRepairs = rand() % 3 + 3; 
	numberExplorers = rand() % 3 + 3; 
	for (i = 0; i < DIMENSION1; i++)
	{
		for (j = 0; j < DIMENSION2; j++)
		{
			reserved[i][j] = false; //arxikopoihsh
		}
	}
}


void map::creation()//arxikopoihsh stoixeiwn xarth
{
	// kairos
	
	for (i = 0; i < DIMENSION1; i++)
	{
		for (j = 0; j < DIMENSION2; j++)
		{
			tempPort = false;
			tempTreasure = false;
			tempWeather = rand() % 10 + 1; 
			tempTreasureAmount = 0;
			position[i][j] = new cell(tempWeather, tempTreasure, tempTreasureAmount, tempPort);//dhmiourgia neou cell
		}
	}
	
	// limani
	
	for (k = 0; k < numberPorts; k++)
	{
		x = rand() % DIMENSION1; //tyxaies times  0-24
		y = rand() % DIMENSION2; //tyxaies times  0-24
		while (reserved[x][y] == true) //periptwsh pou h thesh einai piasmenh
		{
			x = rand() % DIMENSION1; 
			y = rand() % DIMENSION2; 
		}
		reserved[x][y] = true;//kane reserved th thesh
		tempWeather = position[x][y]->getWeather();	
		tempPort = true;
		tempTreasure = false;
		tempTreasureAmount = 0;
		position[x][y] = new cell(tempWeather, tempTreasure, tempTreasureAmount, tempPort);//dhmiourgia neou cell
	}
	
	//thisauros

	tempTotalTreasure = treasureTotal;
	while (tempTotalTreasure > 0)//skorpisma thhsaurou se shmeia sto xarth
	{
		tempTreasureAmount = rand() % 5 + 1; 
		if (tempTotalTreasure - tempTreasureAmount >= 0 )
			tempTotalTreasure -= tempTreasureAmount;
		if (tempTreasureAmount > tempTotalTreasure )
			{tempTreasureAmount = tempTotalTreasure;
			 tempTotalTreasure = 0;}
			 
		x = rand() % DIMENSION1; 
		y = rand() % DIMENSION2; 
		while (reserved[x][y] == true) //periptwsh pou h thesh einai piasmenh
		{
			x = rand() % DIMENSION1; 
			y = rand() % DIMENSION2; 
		}
		tempTreasure = true;
		tempWeather = position[x][y]->getWeather();
		tempPort = false;
		position[x][y] = new cell(tempWeather, tempTreasure, tempTreasureAmount, tempPort);//dhmiourgia neou cell
	}
	
	//ships
	
	id = 0;
	for (i = 0; i < numberPirates ; i++)
	{
		ID=id++;
        allShips.push_back (new Pirate( ID, reserved) );//dhmiourgia pirates	
		
	}
	cout << "  " << numberPirates  << " PIRATE SHIPS HAVE BEEN CREATED" << endl;
	
	id = 0;
	for (i = 0 ; i < numberMerchants; i++)
	{
		ID=id++;
		allShips.push_back (new Merchant( ID, reserved) );//dhmiourgia merchants	
	}
	cout << "  " << numberMerchants << " MERCHANT SHIPS HAVE BEEN CREATED" << endl;
	
	id = 0;
	for (i = 0; i < numberRepairs; i++)
	{
		ID=id++;
		allShips.push_back (new Repair( ID, reserved) );//dhmiourgia repairs
	}	
	cout << "  " << numberRepairs << " REPAIR SHIPS HAVE BEEN CREATED" << endl;

	id = 0;
	for (i = 0; i < numberExplorers; i++)
	{
		ID=id++;
		allShips.push_back (new Explore( ID, reserved) );//dhmiourgia explorers
	}
	cout << "  " << numberExplorers << " EXPLORER SHIPS HAVE BEEN CREATED" << endl << endl;
	
    cout << "  PRESS ANY KEY TO CONTINUE..." << endl << endl;
	getch();
	
	system("cls");
	drawMap();//emfanish aparaithtwn mhnumatwn
	cout << endl << endl;
	cout << "  WE HAVE SHIPS AND SEA NOW!" << endl << endl;
	cout << "  ALL SHIPS ARE RED" << endl << endl;
	cout << "  ALL PORTS ARE GREY" << endl << endl;
	cout << "  ALL TREASURES ARE YELLOW" << endl << endl;
	cout << "  'P' = PIRATE SHIPS" << endl;
	cout << "  'M' = MERCHANT SHIPS" << endl;
	cout << "  'R' = REPAIR SHIPS" << endl;
	cout << "  'E' = EXPLORER SHIPS" << endl << endl;
    cout << "  PRESS ANY KEY TO CONTINUE" << endl << endl;
	getch();
    application();//ektelesh efarmoghs
}

void map::application()//ektelesh efarmoghs
{		

    while ( allShips.size() > 0 ) 
    {
		// se periptwsh p ena ploio gemisei me thusauro termatizei h prosomoiwsh
		for (i = 0; i < allShips.size(); i++)
		{
			if ( (allShips[i]->getCategory() == 'P' &&  allShips[i]->getTreasure()  >= allShips[i]->getMaxTreasure() )
			||(allShips[i]->getCategory() == 'M' &&  allShips[i]->getTreasure()  >=  allShips[i]->getMaxTreasure() )
			|| (allShips[i]->getCategory() == 'R' &&  allShips[i]->getTreasure()  >=  allShips[i]->getMaxTreasure() )
			|| (allShips[i]->getCategory() == 'E' &&  allShips[i]->getTreasure()  >=  allShips[i]->getMaxTreasure() ) )
				endApplication(i);
		}
		
		//leitourgia ploiwn , elegxos kai kinhsh
		for (int i = 0; i < allShips.size(); i++)
		{
			allShips[i]->movement(allShips, i, reserved);//kinhsh ploiwn
			controlShips(i);//elegxos ploiwn
		}
		 for (int i = 0; i < allShips.size(); i++)
		{
			  allShips[i]->operation(i, allShips, position, reserved);//leitourgia ploiwn
		}
	
		drawMap();//emfanish ananewmenou xarth
		
		RoundCount++;//metrhsh round paixnidiou
		cout << "  ROUND " << RoundCount << endl <<endl;
		cout << "  PRESS ANY KEY TO PAUSE THE APPLICATION" << endl;
		
	    if( kbhit() ) //an exei paththei plhktro emfanizei mhnuma epilogwn g xrhsth
		{	
			getch();
			cout << endl;
			cout << " IF YOU WANT TO CONTINUE THE GAME, TYPE C. IF YOU WANT TO GO TO THE MENU, TYPE M "  << endl ;
			cin >> choice;
			while (choice != "M" && choice != "C")
			{
				cout << " TRY AGAIN... TYPE C OR M !! FOR GOD SAKE!!!!" << endl;
				cin >> choice;
			}
			if (choice == "M")
				menu();
			else
				application();
		}
		
		//kairos
		for (k = 0; k < DIMENSION1; k++)
		{
			for (l = 0; l < DIMENSION2; l++)
			{
				temp = position[k][l]->getWeather();
				random = rand() % 2;//allagh timhs kairou se kathe round me tyxaio tropo

				if (random == 1)
				{
					if (temp < 10)
						temp++;
					else
						temp--;
				}
				else
				{
					if (temp > 1)
						temp--;
					else
						temp++;			
				}
				position[k][l]->setWeather(temp);

			}
		}
    }
	endApplication(i);
}

// periptwseis : den uparxoun ploia h' kapoio ploio exei gemisei me thisauro
void map::endApplication(int i)//termatismos application
{
	cout << endl << endl;
	if( allShips.size() < 1 )//den yparxoun ploia
	{
		cout << "  THERE IS NO SHIP LEFT." << endl;
		cout << endl <<endl;
		cout << " IF YOU WANT TO RESTART THE GAME, TYPE R. IF YOU WANT TO STOP THE GAME (WE HOPE TO SEE YOU AGAIN), TYPE S "  << endl ;
		cin >> choice;
		while (choice != "R" && choice != "S")
		{
			cout << " TRY AGAIN... TYPE R OR S !! FOR GOD SAKE!!!!" << endl;
			cin >> choice;
		}
		if (choice == "R")
			restart();
		else 
			cout << "  END OF GAME . PRESS ANY KEY TO EXIT." << endl;
	}
	else //ploio exei gemisei me megisto thhsauro
	{
		cout << "  SHIP " << allShips[i]->getCategory() <<  allShips[i]->getId() << " HAS THE MAXIMUM AMOUNT OF TREASURE";
		cout << endl <<endl;
		cout << " IF YOU WANT TO RESTART THE GAME, TYPE R. IF YOU WANT TO STOP THE GAME (WE HOPE TO SEE YOU AGAIN), TYPE S "  << endl ;
		cin >> choice;
		while (choice != "R" && choice != "S")
		{
			cout << " TRY AGAIN... TYPE R OR S !! FOR GOD SAKE!!!!" << endl;
			cin >> choice;
		}
		if (choice == "R")
			restart();
		else 
			cout << "  END OF GAME . PRESS ANY KEY TO EXIT. " << endl;
	}
	getch();
	exit(0);
}

void map::restart()
{
	allShips.clear();
	system("cls");
	main();
}

void  map::menu()//emfanish menu
{
    system("cls");
    drawMap();
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
    cout << endl <<endl;
    cout << "  Menu" <<endl;
    for (i = 0; i < DIMENSION1; i++)
        cout << "*";
    cout << endl <<endl;
    cout << "  [1]  INFORMATION ABOUT A SHIP" << endl;
    cout << "  [2]  POSITION INFORMATION" << endl;
    cout << "  [3]  SHIP CATEGORY INFORMATION" << endl;
    cout << "  [4]  GENERAL INFORMATION" << endl;
	cout << "  [5]  CREATE A SHIP" << endl;
	cout << "  [6]  DESTROY A SHIP" << endl;
	cout << "  [7]  EDIT MAP POSITION" << endl;
    cout << "  [8]  CONTINUE APPLICATION" << endl;
    cout << "  [9]  RESTART APPLICATION" << endl;
    cout << "  [10]  STOP APPLICATION" << endl;
      
    cin >> input;
    
    if (input == 1)//ektelesh epiloghs xrhsth
		shipInformation();
	else if (input == 2)
		positionInformation();
	else if (input == 3)
		shipCategoryInformation();
	else if (input == 4)
		generalInformation();
	else if (input == 5)
		createShip();
	else if (input == 6)
		destroyShip();
	else if (input == 7)
		editPosition();
	else if (input == 8)
		application();
	else if (input == 9)
		restart();
	else if (input == 10)
	{
		cout << "  APPLICATION IS DEAD. PRESS ANY KEY TO EXIT" << endl;
		getch();
		exit(0);
	}
	else 
    {
        cout << "  TRY AGAIN... TYPE 1,2,3,4,5,6,7,8,9 OR 10 !! FOR GOD SAKE!!!!I GIVE YOU SO MANY OPTIONS!"  << endl;
		Sleep(1500);
		menu();
	}   
}

// epilogh 1 apo plhktrologio
void  map::shipInformation()//plhrofories gia ena ploio
{

	
	system("cls");
	drawMap();
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
 	cout << endl << endl;
	cout << "  SHIP INFORMATION" << endl;
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	cout << "  TYPE CATEGORY OF SHIP:" << endl;
    cout << "  P = PIRATE, M = MERCHANT, R = REPAIR, E = EXPLORER " << endl;
	cin >> shipCategoryIn;//dialegei kathgoria kai epeita id o xrhsths
	
	if (shipCategoryIn == 'P')
	{
		cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberPiratesTotal << endl;
		cin >>  shipIdIn;        
		while (shipIdIn < 0 || shipIdIn > numberPirates )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 - " << numberPiratesTotal << endl;
			cin >>  shipIdIn; 
		}
	}
	else if (shipCategoryIn == 'M')
	{
        cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberMerchantsTotal << endl;
		cin >> shipIdIn; 
	    while (shipIdIn < 0 || shipIdIn > numberMerchants )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 -  " << numberMerchantsTotal  << endl;
			cin >> shipIdIn; 
		}
    }
	else if (shipCategoryIn == 'R')
	{
        cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberRepairsTotal << endl;
		cin >> shipIdIn; 
		while (shipIdIn < 0 || shipIdIn > numberRepairs )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 -  " << numberRepairsTotal << endl;
			cin >> shipIdIn; 
		}
    }
	else if (shipCategoryIn == 'E')
	{
        cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberExplorersTotal << endl;
		cin >> shipIdIn; 
		while (shipIdIn < 0 || shipIdIn > numberExplorers )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 -  " << numberExplorersTotal << endl;
			cin >> shipIdIn; 
		}
    }
	else
	{
		cout << " TRY AGAIN... TYPE P, M, R OR E !! FOR GOD SAKE!!" << endl;
		Sleep(1500);
		shipInformation();
	}
	flag=false;
	for (i = 0; i < allShips.size(); i++)
	{
		if (allShips[i]->getCategory() == shipCategoryIn && allShips[i]->getId() == shipIdIn )//emfanish antistoixwn plhroforiwn
		{	
			flag = true;		   
            cout << "  CATEGORY: " << allShips[i]->getCategory() << endl;
            cout << "  ID: " << allShips[i]->getId() << endl;
			cout << "  SPEED: " << allShips[i]->getSpeed()  << endl;
			cout << "  TREASURE: " << allShips[i]->getTreasure()  << endl;
			cout << "  SHIP POSITION: " << allShips[i]->getX()  << "," << allShips[i]->getY() << endl;
			cout << "  MAXIMUM LIFE: " << allShips[i]->getMaxLife()  << endl;
			cout << "  CURRENT LIFE: " << allShips[i]->getCurrentLife()  << endl;
			cout << "  NUMBER OF MOVEMENTS: " << allShips[i]->getTotalMoves() << endl;
			if (allShips[i]->getCategory() == 'P')
				cout << "  TOTAL DAMAGE TO OTHER SHIPS: " << allShips[i]->getDamage()  << endl;
			cout << endl << endl;
			cout << "  OTHER OPTIONS: "  << endl ;
			cout << "  A = INFO ABOUT ANOTHER SHIP" << endl;
			cout << "  M = MENU" << endl;
			cout << "  C = APPLICATION" << endl;
			cout << endl << endl;
			cin >> choice;
			while (choice != "M" && choice != "C" && choice != "A")
			{
				cout << "  TRY AGAIN... TYPE A, M OR C !! FOR GOD SAKE!!" << endl;
				cin >> choice;
			}
			if (choice == "A")
				shipInformation();
			else if (choice == "M")
				menu();
			else 
				application();
		}
		
		/*else{cout << "THIS SHIP IS DESTROYED" << endl;
			
		}*/
	}
	 if(flag==false) {//an to ploio exei katastrafei
			cout << " SHIP "<< shipCategoryIn <<shipIdIn <<" HAS BEEN DESTROYED" <<endl << endl;
			cout << "  OTHER OPTIONS: "  << endl ;
			cout << "  A = INFO ABOUT ANOTHER SHIP" << endl;
			cout << "  M = MENU" << endl;
			cout << "  C = APPLICATION" << endl;
			cout << endl << endl;
			cin >> choice;
			while (choice != "M" && choice != "C" && choice != "A")
			{
				cout << "  TRY AGAIN... TYPE A, M OR C !! FOR GOD SAKE!!" << endl;
				cin >> choice;
			}
			if (choice == "A")
				shipInformation();
			else if (choice == "M")
				menu();
			else 
				application();
			
		}
}

//epilogh 2 apo plhktrologio
void map::positionInformation()//plhrofories gia mia thesh tou xarth
{
	system("cls");
	drawMap();
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	cout << "  MAP POSITION INFORMATION" << endl;
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	
	//epilogh syntetagmenwn shmeiou
	cout << "  TYPE A NUMBER BETWEEN 0 - " << DIMENSION1-1  << endl;
	cin >> xIn;
	while ( xIn < 0 || xIn >= DIMENSION1)
	{
		cout << "  TRY AGAIN... TYPE A NUMBER BETWEEN 0 - " << DIMENSION1-1   << endl;
		cin >> xIn;
	} 
	
	cout << "  TYPE A NUMBER BETWEEN 0 - " << DIMENSION2-1 << endl;	
	cin >> yIn; 
	while ( yIn < 0 || yIn >= DIMENSION2)
	{ 
		cout << "  TYPE A NUMBER BETWEEN 0 - " << DIMENSION2-1 << endl;
		cin >> yIn;
	} 

	system("cls");
	drawMap();
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	cout << "  MAP POSITION INFORMATION" << endl << endl;
	cout << "  ";
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	//emfanish plhroforiwn shmeiou
	cout << " WEATHER: " << position[xIn][yIn]->getWeather() << endl;
	cout << " TREASURE : " << position[xIn][yIn]->getTreasure() << endl;
	cout << " PORT: ";
	if (position[xIn][yIn]->getPort() == true)
		cout << " YES" << endl;
	else
		cout << " NO" << endl;
		
	cout << endl << endl;
			cout << "  OTHER OPTIONS: "  << endl ;
			cout << "  P = INFO ABOUT ANOTHER POSITION" << endl;
			cout << "  M = MENU" << endl;
			cout << "  C = APPLICATION" << endl;
			cout << endl << endl;
			cin >> choice;
			while (choice != "M" && choice != "C" && choice != "P")
			{
				cout << "  TRY AGAIN... TYPE P, M OR C !! FOR GOD SAKE!!" << endl;
				cin >> choice;
			}
			if (choice == "P")
				positionInformation();
			else if (choice == "M")
				menu();
			else 
				application();
}
//epilogh 3 apo plhktrologio
void map:: shipCategoryInformation()//plhrofories gia mia kathgoria ploiou
{
    system("cls");
    drawMap();
    for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	cout << "  SHIP CATEGORY INFORMATION" << endl << endl;
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";	
	cout << endl << endl;
	cout << "  SELECT SHIP CATEGORY:" << endl;
    cout << "  P = PIRATE, M = MERCHANT, R = REPAIR, E = EXPLORER " << endl;//epilogh kathgorias
	cin >> shipCategoryIn;

    system("cls");
    drawMap();
    for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;//emfanish antistoixwn plhroforiwn
	cout << "  SHIP CATEGORY INFORMATION" << endl << endl;
    cout << "  ";
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";	
	cout << endl << endl;

	int total;	
	if (shipCategoryIn == 'P')
	{
		total = 0;
		for (i = 0; i < numberPirates;  i++)
		{
			temp = allShips[i]->getTreasure();//metrhsh thhsaurou kathgorias
			total += temp;
		}
		cout << "  THERE ARE " << numberPirates  << " PIRATE SHIPS" << endl;
		cout << "  WITH TOTAL TREASURE: " << total << endl;	

	}
	else if (shipCategoryIn == 'M')
	{
		total = 0;
		for (i = numberPirates; i <(numberPirates + numberMerchants);  i++)
		{
			temp = allShips[i]->getTreasure();//metrhsh thhsaurou kathgorias
			total += temp;
		}
		cout << "  THERE ARE " << numberMerchants  << " MERCHANT SHIPS" << endl;
		cout << "  WITH TOTAL TREASURE: " << total << endl;	

    }
	else if (shipCategoryIn == 'R')
	{
		total = 0;
		for (i = numberMerchants; i < (numberPirates + numberMerchants + numberRepairs);  i++)
		{
			temp = allShips[i]->getTreasure();//metrhsh thhsaurou kathgorias
			total += temp;
		}
		cout << "  THERE ARE " << numberRepairs  << " REPAIR SHIPS" << endl;
		cout << "  WITH TOTAL TREASURE: " << total << endl;	
    }
	else if (shipCategoryIn == 'E')
	{
		total = 0;
		for (i = numberRepairs; i < allShips.size();  i++)
		{
			temp = allShips[i]->getTreasure();//metrhsh thhsaurou kathgorias
			total += temp;
		}
		cout << "  THERE ARE " << numberExplorers  << " EXPLORERS SHIPS" << endl;
		cout << "  WITH TOTAL TREASURE: " << total << endl;	
    }
	else
	{
		cout << "  WRONG INPUT." << endl;
		Sleep(1500);
		shipCategoryInformation();
	}
	
		cout << endl << endl;
			cout << "  OTHER OPTIONS: "  << endl ;
			cout << "  A = INFO ABOUT ANOTHER CATEGORY OF SHIPS" << endl;
			cout << "  M = MENU" << endl;
			cout << "  C = APPLICATION" << endl;
			cout << endl << endl;
			cin >> choice;
			while (choice != "M" && choice != "C" && choice != "A")
			{
				cout << "  TRY AGAIN... TYPE A, M OR C !! FOR GOD SAKE!!" << endl;
				cin >> choice;
			}
			if (choice == "A")
				shipCategoryInformation();
			else if (choice == "M")
				menu();
			else 
				application();
}

//epilogh 4 apo plhktrologio
void map::generalInformation()//genikes plhrofories
{
    system("cls");
    drawMap();
    for (i = 0; i < DIMENSION1; i++)
        cout << "*";
    cout << endl << endl;
	cout << "  GENERAL INFORMATION" << endl << endl;
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";	
	cout << endl << endl;

	max = 0;
	for (i = 0; i < allShips.size();  i++)
	{
		if (allShips[i]->getTreasure()  > max) 
			max = allShips[i]->getTreasure();
	}
			
	cout << "  SHIPS WITH THE MOST TREASURE AMOUNT"  << endl;//emfanish ploiwn me megalytero thhsauro
	for (i = 0; i < allShips.size();  i++)
	{
		if (allShips[i]->getTreasure() == max)
		cout << " ARE " << allShips[i]->getCategory() << allShips[i]->getId()  << endl; 
	}
	cout << endl;
	for (i = 0; i < allShips.size();  i++)
	{
		cout << "  SHIP " << allShips[i]->getCategory() << allShips[i]->getId();
		cout << " HAS " << allShips[i]->getTreasure() << endl;
	}
	cout << endl << endl;
	cout << "  OTHER OPTIONS:"  << endl ;
	cout << "  M = MENU" << endl;
	cout << "  C = APPLICATION" << endl;
	cout  << endl << endl <<   endl;
	cin >> choice;
	while (choice != "M" && choice != "C")
	{
		cout << "  TRY AGAIN... TYPE M OR C !! FOR GOD SAKE!!" << endl;
		cin >> choice;
	}
	if (choice == "M")
		menu();
    else
		application();
}

//epilogh 5 apo plhktrologio
void map::createShip()//dhmiourgia ploiou
{
    system("cls");
    drawMap();
    for (i = 0; i < DIMENSION1; i++)
        cout << "*";
    cout << endl << endl;
	cout << "  SHIP CREATION" << endl << endl;
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";	
	cout << endl << endl;
	cout << "  TYPE A SHIP CATEGORY:" << endl;//epilogh kathgorias ploiou pou tha dhmiourghthei
    cout << "   P = PIRATE, M = MERCHANT, R = REPAIR, E = EXPLORER " << endl;
	cin >> shipCategoryIn;
	if (shipCategoryIn == 'P')
	{
		allShips.insert( allShips.begin() + numberPirates , new Pirate (++numberPirates , reserved) );
		//numberPirates++;
		numberPiratesTotal++;//aukshsh arithmou ploiwn
		cout << endl << "  YOU CREATED PIRATE SHIP " << "P" << numberPirates<< endl;
	    Sleep(1500);
	}
	else if (shipCategoryIn == 'M')
	{
		allShips.insert( allShips.begin() + (numberPirates + numberMerchants), new Merchant (++numberMerchants, reserved ) );
		//numberMerchants++;
		numberMerchantsTotal++;//aukshsh arithmou ploiwn
		cout << endl << "  YOU CREATED MERCHANT SHIP " << "M" << numberMerchants << endl;
	    Sleep(1500);
	}
	else if (shipCategoryIn == 'R')
	{
		allShips.insert( allShips.begin() + (numberPirates + numberMerchants + numberRepairs), new Repair (++numberRepairs, reserved ) );
		//numberRepairs++;
		numberRepairsTotal++;//aukshsh arithmou ploiwn
		cout << endl << "  YOU CREATED REPAIR SHIP " << "R" << numberRepairs<< endl;
	    Sleep(1500);
	}
	else if (shipCategoryIn == 'E')
	{
		allShips.insert( allShips.begin() + allShips.size(), new Explore (++numberExplorers, reserved ) );
		//numberExplorers++;
		numberExplorersTotal++;//aukshsh arithmou ploiwn
		cout << endl << "  YOU CREATED EXPLORER SHIP " << "E" << numberExplorers << endl;
	    Sleep(1500);		
	}
	else
	{
		cout << "  WRONG INPUT." << endl;
		Sleep(1500);
		createShip();
	}

	cout << "  OTHER OPTIONS: "  << endl ;
	cout << "  A = ANOTHER CREATION" << endl;
    cout << "  M = MENU" << endl;
	cout << "  C = APPLICATION" << endl;
	cout << endl << endl;
	cin >> choice;
	while (choice != "M" && choice != "C" && choice != "A")
		{
			cout << "  TRY AGAIN... TYPE A, M OR C !! FOR GOD SAKE!!" << endl;
			cin >> choice;
		}
	if (choice == "A")
		createShip();
	else if (choice == "M")
		menu();
	else 
		application();
}

//epilogh 6 apo plhktrologio
void map::destroyShip()//katastrofh ploiou
{
    system("cls");
    drawMap();
    for (i = 0; i < DIMENSION1; i++)
        cout << "*";
    cout << endl << endl;
	cout << " DESTROY SHIP" << endl << endl;
	cout << "  ";
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";	
	cout << endl << endl;

	cout << "  SELECT SHIP CATEGORY:" << endl;//epilogh kathgorias ploiou pou tha katastrafei
    cout << "  P = PIRATE, M = MERCHANT, R = REPAIR, E = EXPLORER"  << endl;
	cin >> shipCategoryIn;
	if (shipCategoryIn == 'P')
	{
		cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberPirates-1 << endl;//epilogh id ploiou pou tha katastrafei
		cin >>  shipIdIn;        
		while (shipIdIn < 0 || shipIdIn > numberPirates )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 - " << numberPirates-1 << endl;
			cin >>  shipIdIn; 
		}
	
		
		for (i = 0; i < allShips.size(); i++)
		{
			if ( allShips[i]->getCategory() == shipCategoryIn && allShips[i]->getId() == shipIdIn )
			{
				cout << "  SHIP " <<  shipCategoryIn << shipIdIn << " HAS BEEN DESTROYED" << endl;
				Sleep(500);
				reserved[allShips[i]->getX()][allShips [i]->getY()] = false;//eleutherwsh theshs ploiou
				allShips.erase( allShips.begin() + i );//diagrafh ploiou
				numberPirates--;//meiwsh arithmou ploiwn

			}			
		}
	}
	else if (shipCategoryIn == 'M')
	{
		cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberMerchants-1 << endl;
		cin >> shipIdIn; 
	    while (shipIdIn < 0 || shipIdIn > numberMerchants )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 -  " << numberMerchants-1  << endl;
			cin >> shipIdIn; 
		}
		for (i = 0; i < allShips.size(); i++)
		{
			if ( allShips[i]->getCategory() == shipCategoryIn && allShips[i]->getId() == shipIdIn )
			{
				cout << "  SHIP " <<  shipCategoryIn << shipIdIn << " HAS BEEN DESTROYED" << endl;
				Sleep(500);
				reserved[ allShips[i]->getX() ] [allShips [i]->getY() ] = false;	//eleutherwsh theshs ploiou			
				allShips.erase( allShips.begin() + i );//diagrafh ploiou
				numberMerchants--;//meiwsh arithmou ploiwn
			}			
		}

	}
	else if (shipCategoryIn == 'R')
	{
		cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberRepairs-1 << endl;
		cin >> shipIdIn; 
		while (shipIdIn < 0 || shipIdIn > numberRepairs )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 -  " << numberRepairs-1 << endl;
			cin >> shipIdIn; 
		}
		for (i = 0; i < allShips.size(); i++)
		{
			if ( allShips[i]->getCategory() == shipCategoryIn && allShips[i]->getId() == shipIdIn )
			{
				cout << "  SHIP " <<  shipCategoryIn << shipIdIn << " HAS BEEN DESTROYED" << endl;
				Sleep(500);
				reserved[ allShips[i]->getX() ] [allShips [i]->getY() ] = false;	//eleutherwsh theshs ploiou			
				allShips.erase( allShips.begin() + i);//diagrafh ploiou
				numberRepairs--;//meiwsh arithmou ploiwn
			}				
		}
	}
	else if (shipCategoryIn == 'E')
	{
		cout << "  TYPE ID OF SHIP BETWEEN 0 - " << numberExplorers-1 << endl;
		cin >> shipIdIn; 
		while (shipIdIn < 0 || shipIdIn > numberExplorers )
		{	
			cout << "  WRONG INPUT. TYPE ID OF SHIP BETWEEN 0 -  " << numberExplorers-1 << endl;
			cin >> shipIdIn; 
		}
		for (i = 0; i < allShips.size(); i++)
		{
			if ( allShips[i]->getCategory() == shipCategoryIn && allShips[i]->getId() == shipIdIn )
			{
				cout << "  SHIP " <<  shipCategoryIn << shipIdIn << " HAS BEEN DESTROYED" << endl;
				Sleep(500);
				reserved[allShips[i]->getX() ] [allShips [i]->getY() ] = false;		//eleutherwsh theshs ploiou		
				allShips.erase( allShips.begin() + i );//diagrafh ploiou
				numberExplorers--;//meiwsh arithmou ploiwn
			}	
		}
	}
	else
	{
        cout << "  WRONG INPUT." << endl;
		Sleep(1500);
		destroyShip();
	}
	cout << "  OTHER OPTIONS: "  << endl ;
	cout << "  D = ANOTHER DESTROY" << endl;
    cout << "  M = MENU" << endl;
	cout << "  C = APPLICATION" << endl;
	cout << endl << endl;
	cin >> choice;
	while (choice != "M" && choice != "C" && choice != "D")
		{
			cout << "  TRY AGAIN... TYPE D, M OR C !! FOR GOD SAKE!!" << endl;
			cin >> choice;
		}
	if (choice == "D")
		destroyShip();
	else if (choice == "M")
		menu();
	else 
		application();
}

//epilogh 7 apo plhktrologio
void map::editPosition()//allagh stoixeiwn shmeiou
{
	system("cls");
	drawMap();
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	cout << "  EDIT POSITION" << endl;
	cout << "  ";
	for (i = 0; i < DIMENSION1; i++)
        cout << "*";
	cout << endl << endl;
	
	//epilogh syntetagmenwn shmeiou
	cout << "  TYPE A NUMBER BETWEEN 0 - " << DIMENSION1-1  << endl;
	cin >> xIn;
	while ( xIn < 0 || xIn >= DIMENSION1)
	{
		cout << "  TRY AGAIN... TYPE A NUMBER BETWEEN 0 - " << DIMENSION1-1   << endl;
		cin >> xIn;
	} 
	
	cout << "  TYPE A NUMBER BETWEEN 0 - " << DIMENSION2-1 << endl;	
	cin >> yIn; 
	while ( yIn < 0 || yIn >= DIMENSION2)
	{ 
		cout << "  TYPE A NUMBER BETWEEN 0 - " << DIMENSION2-1 << endl;
		cin >> yIn;
	}  
	
	cout << "  DO YOU WANT TO SET THIS POSITION AS A PORT? SELECT (Y) FOR YES AND (N) FOR NO!" << endl;
	cin >> choice;
	while(choice != "Y" && choice != "N")
	{
		cout << "  TYPE 'Y' OR 'N'" << endl;
		cin >> choice;
	}
	if (choice == "Y")	//dhmiourgia limaniou sto shmeio
	{
		//San na fainetai na yparxei ena ploio edw
		if (reserved[xIn][yIn] == true && position[xIn][yIn]->getPort() == false)
		{
			cout << "  WE HAVE A SHIP IN THIS POSOTION! TRY AGAIN LATER" << endl;
			Sleep(1500);
			
			cout << "  OTHER OPTIONS: "  << endl ;
			cout << "  E = ANOTHER EDIT" << endl;
			cout << "  M = MENU" << endl;
			cout << "  C = APPLICATION" << endl;
			cout << endl << endl;
			cin >> choice;
			while (choice != "M" && choice != "C" && choice != "E")
				{
					cout << "  TRY AGAIN... TYPE E, M OR C !! FOR GOD SAKE!!" << endl;
					cin >> choice;
				}
			if (choice == "E")
				editPosition();
			else if (choice == "M")
				menu();
			else 
				application();
				}
		else // diathesimh thesh
		{
			position[xIn][yIn]->setPort(true);
			reserved[xIn][yIn] = true;
			cout << "  POSITION " << xIn << "," << yIn << " IS NOW A PORT" << endl;
			Sleep(1500);
		}
	}
	else if (choice == "N")	//diagrafh limaniou sto shmeio
	{
		if (position[xIn][yIn]->getPort() == false && reserved[xIn][yIn] == true) // ekei uparxei ena ploio
			reserved[xIn][yIn] = true;
		else
		{
			position[xIn][yIn]->setPort(false);
			reserved[xIn][yIn] = false;
		}
		cout << "  SET VALUE FOR WEATHER! TYPE A NUMBER BETWEEN 1 AND 10" << endl;//these entash kairou sto shmeio
		cin >> newWeather;
		while ( newWeather < 0 || newWeather > 10)
	{
		cout << "  TRY AGAIN... TYPE A NUMBER BETWEEN 0 - 10"  << endl;
		cin >> newWeather;
	} 
		
		position[xIn][yIn]->setWeather(newWeather);
		cout << "  SELECT TREASURE BETWEEN 0 AND 4)" << endl;//these thhsauro sto shmeio
		cin >> newTreasure;
		while ( newTreasure < 0 || newTreasure > 4)
	{
		cout << "  TRY AGAIN... TYPE A NUMBER BETWEEN 0 - 4"  << endl;
		cin >> newTreasure;
	} 
		
		if (newTreasure > 0)
		   	position[xIn][yIn]->setTreasureAmount(true);
		position[xIn][yIn]->setTreasure(newTreasure);
		cout << "  POSITION HAS BEEN EDITED" << endl;
		Sleep(1500);
	}
	
	cout << "  OTHER OPTIONS: "  << endl ;
	cout << "  E = ANOTHER EDIT" << endl;
	cout << "  M = MENU" << endl;
	cout << "  C = APPLICATION" << endl;
	cout << endl << endl;
	cin >> choice;
	while (choice != "M" && choice != "C" && choice != "E")
		{
			cout << "  TRY AGAIN... TYPE E, M OR C !! FOR GOD SAKE!!" << endl;
			cin >> choice;
		}
	if (choice == "E")
		editPosition();
	else if (choice == "M")
		menu();
	else 
		application();
		}
		
//diaxeirhsh ploiwn
void map::controlShips(int i)
{
	// meiwsh antoxhs ploiou se periptwsh kairou > 6

		if (position[allShips[i]->getX()][allShips[i]->getY()]->getWeather() > 6)
		{
			temp = allShips[i]->getCurrentLife();
			temp--;
			allShips[i]->setCurrentLife(temp);
			cout << "  SHIP " <<   allShips[i]->getCategory() << allShips[i]->getId() << " HAS LOST 1 LIFE BECAUSE OF BAD WEATHER" << endl;
			Sleep (500);
		}
	
	// auksish apothematwn xrusou enos ploiou
	if (position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount() > 0)
	{
		if (allShips[i]->getCategory() == 'P' && allShips[i]->getMoving()  == true) 
		{
			if (allShips[i]->getTreasure()  == allShips[i]->getMaxTreasure())
			{
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS FULL TREASURE AMOUNT" << endl;
				endApplication(i);
			}
			else if (allShips[i]->getTreasure() < allShips[i]->getMaxTreasure())
			{
				temp = allShips[i]->getTreasure();
				temp++;
				allShips[i]->setTreasure(temp);// auksish apothematwn xrusou
				temp = position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount();
				temp--;
				position[allShips[i]->getX()][allShips[i]->getY()]->setTreasureAmount(temp);// meiwsh apothematwn xrusou shmeiou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISE ITS TREASURE BY 1" << endl;
				Sleep (500);
			}
		}
		else if (allShips[i]->getCategory() == 'M' && allShips[i]->getMoving()  == true) 
		{
			if (allShips[i]->getTreasure()  == allShips[i]->getMaxTreasure())
			{
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS FULL TREASURE AMOUNT" << endl;
				Sleep (500);
				endApplication(i);
			}
			else if (allShips[i]->getTreasure() < allShips[i]->getMaxTreasure())
			{
				temp = allShips[i]->getTreasure();
				temp++;
				allShips[i]->setTreasure(temp);// auksish apothematwn xrusou
				temp = position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount();
				temp--;
				position[allShips[i]->getX()][allShips[i]->getY()]->setTreasureAmount(temp);// meiwsh apothematwn xrusou shmeiou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISE ITS TREASURE BY 1" << endl;
				Sleep (500);
			}
		}
		else if (allShips[i]->getCategory() == 'R' && allShips[i]->getMoving()  == true) 
		{
			if (allShips[i]->getTreasure()  == allShips[i]->getMaxTreasure())
			{
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS FULL TREASURE AMOUNT" << endl;
				Sleep (500);
				endApplication(i);
			}
			else if (allShips[i]->getTreasure() < allShips[i]->getMaxTreasure())
			{
				temp = allShips[i]->getTreasure();
				temp++;
				allShips[i]->setTreasure(temp);// auksish apothematwn xrusou
				temp = position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount();
				temp--;
				position[allShips[i]->getX()][allShips[i]->getY()]->setTreasureAmount(temp);// meiwsh apothematwn xrusou shmeiou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISE ITS TREASURE BY 1" << endl;
				Sleep (500);
			}
		}
		else if (allShips[i]->getCategory() == 'E' && allShips[i]->getMoving()  == true) 
		{
			if (allShips[i]->getTreasure()  == allShips[i]->getMaxTreasure())
			{
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS FULL TREASURE AMOUNT" << endl;
				Sleep (500);
				endApplication(i);
			}
			else if (allShips[i]->getTreasure() < allShips[i]->getMaxTreasure())
			{
				temp = allShips[i]->getTreasure();
				temp++;
				allShips[i]->setTreasure(temp);// auksish apothematwn xrusou
				temp = position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount();
				temp--;
				position[allShips[i]->getX()][allShips[i]->getY()]->setTreasureAmount(temp);// meiwsh apothematwn xrusou shmeiou
				cout << "  SHIP " <<  allShips[i]->getCategory() << allShips[i]->getId() << " HAS RAISE ITS TREASURE BY 1" << endl;
				Sleep (500);
			}
		}
		if (position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount() == 0)
		{
			position[allShips[i]->getX()][allShips[i]->getY()]->setTreasure(false);
			cout << "  POSITION " <<  allShips[i]->getX()<<"," << allShips[i]->getY() << " HAS NO TREASURE ANYMORE" << endl;}
			Sleep (500);
		}

	//an kapoio ploio exei zwh = 0 tote katastrefetai
	if (allShips[i]->getCurrentLife()  <= 0) 
	{
		reserved[ allShips[i]->getX() ] [allShips[i]->getY() ] = false;
		if (allShips[i]->getTreasure()  > 0)//dhmiourgia thhsaurou sto shmeio
			position[allShips[i]->getX()][allShips[i]->getY()]->setTreasure(true);
		temp = position[allShips[i]->getX()][allShips[i]->getY()]->getTreasureAmount();
		temp += allShips[i]->getTreasure();
		position[allShips[i]->getX()][allShips[i]->getY()]->setTreasureAmount(temp);  //h thesh pairnei to thhsauro tou ploiou
		if (allShips[i]->getCategory() == 'P')
		{	
			numberPirates--;//meiwsh arithmou ploiwn

		}
		else if (allShips[i]->getCategory() == 'M')
		{	
			numberMerchants--;//meiwsh arithmou ploiwn
		}
		else if (allShips[i]->getCategory() == 'R')
		{
			numberRepairs--;//meiwsh arithmou ploiwn
		}
		else if (allShips[i]->getCategory() == 'E')
		{	
			numberExplorers--;//meiwsh arithmou ploiwn
		}

		cout << endl;
		system("cls");
		drawMap();
		cout << "  SHIP " << allShips[i]->getCategory() << allShips[i]->getId()  << " IS DESTROYED" << endl; 
		allShips.erase( allShips.begin() + i );	//diagrafh ploiou
		Sleep(1500);
	}
}
